import React, { useEffect, useState } from 'react';
import {  BrowserRouter as Router, Route, Switch } from "react-router-dom";
import "antd/dist/antd.css";
import 'font-awesome/css/font-awesome.min.css';
import { ThemeButton } from './components/ThemeButton';
import Content from './components/Content';
import {Description} from './components/Description';
import FourHundredFour from './components/FourHundredFour';
import './styles/theme.scss';
import './App.scss';

function App() {

    const [theme, setTheme] = useState(localStorage.getItem('theme'));

    // Update theme with localStorage

    function updateTheme(theme) {
        if (theme === "dark"){
            setTheme('light');
            localStorage.setItem('theme','light');
            localStorage.setItem('check',true);
        }
        else {
            setTheme('dark');
            localStorage.setItem('theme','dark');
            localStorage.setItem('check',false);
        }
    }
    useEffect(() => {
        if (!localStorage.getItem('theme')){
            localStorage.setItem('theme','light');
        }

        if (localStorage.getItem('theme') === "dark"){
            localStorage.setItem('check',false);
        }
        else
            localStorage.setItem('check',true);
   }, [theme])


    return (
        <Router>
            <div className={`App ${localStorage.getItem('theme')}`}>
                <div className="header">
                    <a href="/" className="title">devjobs</a>
                    <div className="theme">
                    <i className="fas fa-moon"></i>
                    <ThemeButton label="Click Me" onClick={() => updateTheme(theme)} />
                    <i className="fas fa-sun"></i>
                    </div>
                </div>

                <Switch>
                    <Route
                        exact path='/'
                        component={Content}
                    />
                    <Route
                        path='/job/:jobId'
                        component={Description}
                    />
                    <Route path='*' component={FourHundredFour} />
                </Switch>
            </div >
        </Router>
    );
}

export default App;
