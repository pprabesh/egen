import * as actionTypes from '../actions/actionTypes';

const initialState = {
    jobs: [],
    loading: true,
    error: false,
}


const jobReq = (state, action) => {
    return {
        loading: true,
        jobs: [],
    };
}

const jobGet = (state, action) => {

    return {
        ...state,
        loading: false,
        jobs: [...state.jobs, ...action.payload.jobs],
    };
}

const jobErr = (state, action) => {
    return {
        ...state,
        loading: false,
        error: action.payload.error,
        jobs: []
    };
}

const jobNextPage = (state, action) => {
    return {
        ...state,
        loading: false,
        hasNextPage: action.payload.hasNextPage,

    };
}

const jobReducer = (state=initialState, action) => {
    switch (action.type) {
        case actionTypes.JOB_REQUEST: return jobReq(state, action);
        case actionTypes.JOB_GET: return jobGet(state, action);
        case actionTypes.JOB_ERROR: return jobErr(state, action);
        case actionTypes.JOB_HAS_NEXT_PAGE: return jobNextPage(state, action);
        default:
            return state;
    }
}

export default jobReducer;
