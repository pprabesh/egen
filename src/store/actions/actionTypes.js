export const JOB_REQUEST = 'JOB_REQUEST';
export const JOB_GET = 'JOB_GET';
export const JOB_ERROR = 'JOB_ERROR';
export const JOB_HAS_NEXT_PAGE = 'JOB_HAS_NEXT_PAGE';
