import { useReducer, useEffect } from 'react';
import axios from 'axios';
import jobReducer from '../../store/reducers/jobReducer';
import * as actionTypes from './actionTypes';

const API_URL = 'https://afternoon-wave-60119.herokuapp.com/https://jobs.github.com/positions.json'


export default function useJobFetch(params, page, geoLocationPending) {

    const [state, dispatch] = useReducer(jobReducer, {jobs:[], loading: true, error: false})


    useEffect(() => {
        dispatch({ type: actionTypes.JOB_REQUEST })
    }, [params])

    useEffect(() => {

        // Get geolocation first else return
        if (geoLocationPending) return;

        // First request to get page data
        const cancelToken1 = axios.CancelToken.source()
        axios.get(API_URL, {
            cancelToken: cancelToken1.token,
            params: { markdown: true, page: page, ...params }
        }).then(res => {
            if (res.data.error)
            {dispatch({ type: actionTypes.JOB_ERROR, payload: { error: res.data.error } })}
            else  {
            dispatch({ type: actionTypes.JOB_GET, payload: { jobs: res.data }})}
        }).catch(e => {
            if (axios.isCancel(e)) return
            dispatch({ type: actionTypes.JOB_ERROR, payload: { error: e } })
        })

        // Second request to get page number information
        const cancelToken2 = axios.CancelToken.source()
        axios.get(API_URL, {
            cancelToken: cancelToken2.token,
            params: { markdown: true, page: page + 1, ...params }
        }).then(res => {
            dispatch({ type: actionTypes.JOB_HAS_NEXT_PAGE,payload: {hasNextPage: res.data.length !== 0 }})
        }).catch(e => {
            if (axios.isCancel(e)) return
            dispatch({ type: actionTypes.JOB_ERROR, payload: { error: e } })
        })

        return () => {
            cancelToken1.cancel()
            cancelToken2.cancel()
        }
    }, [params, page, geoLocationPending])

    return state
}
