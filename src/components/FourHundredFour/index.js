import React from 'react';
import './styles.scss';

const FourHundredFour = () => {
    return (
        <div className="container">
            <div className="text-box">
                <span className="text">
                    404
                </span>
                <span className="text-2">
                    Page Not Found
                </span>
            </div>
        </div>

    );
}

export default FourHundredFour;
