import React from 'react';
import { Switch } from 'antd';
import './styles.scss';

export const ThemeButton = (props: any) => {
    const { onClick } = props;
    const checked = localStorage.getItem('check');

    return (
        <Switch onChange={onClick} checked={true ? checked === "true" : false} className='theme-btn' / >
    );
};
