import React, { useEffect, useState, useRef, useCallback } from 'react';
import { Link } from "react-router-dom";
import { Skeleton,  Card, Empty } from 'antd';
import moment from 'moment';
import SearchForm from '../../components/SearchForm';
import useJobFetch from '../../store/actions/useJobFetch.js';
import './styles.scss';

const { Meta } = Card;

export default function Content (props) {

    const [data, setData] = useState({});
    const [page, setPageNumber] = useState(1)
    const [cityNamePending, setCityNamePending] = useState(true);
    const { jobs, loading, error, hasNextPage} = useJobFetch(data, page, cityNamePending)


    // Handle Infinite Scroll
    const observer = useRef()
    const lastJobElementRef = useCallback(node => {
        if (loading) return
        if (observer.current) observer.current.disconnect()
        observer.current = new IntersectionObserver(entries => {
            if (entries[0].isIntersecting && hasNextPage) {
                setPageNumber(prevPageNumber => prevPageNumber + 1)
            }
        })
        if (node) observer.current.observe(node)
    }, [loading, hasNextPage])


    // Handle Search
    function handleDataChange(e) {
        const data = e.target.name
        const value = e.target.value
        setPageNumber(1);
        setData(prevData => {
            return { ...prevData, [data]: value }
        })
    }



    useEffect(() => {

        // Get Geo Coordinates and reverse geo encoding to get state name
        function getCity(coordinates) {
            var xhr = new XMLHttpRequest();
            var lat = coordinates[0];
            var lng = coordinates[1];

            xhr.open('GET', " https://us1.locationiq.com/v1/reverse.php?key=pk.7dbea0ef844d8b63a359fca17430c35a&lat=" +
                lat + "&lon=" + lng + "&format=json", true);
            xhr.send();
            xhr.onreadystatechange = processRequest;
            xhr.addEventListener("readystatechange", processRequest, false);

            function processRequest(e) {
                if (xhr.readyState === 4 && xhr.status === 200) {
                    var response = JSON.parse(xhr.responseText);
                    var city = response.address.city;
                    setData({location: city});
                    setCityNamePending(false);
                    return;
                }
            }
        }
        var navOptions = {
            enableHighAccuracy: true,
            timeout: 5000,
            maximumAge: 0
        };

        function navSuccess(pos) {
            var crd = pos.coords;
            var lat = crd.latitude.toString();
            var lng = crd.longitude.toString();
            var coordinates = [lat, lng];
            getCity(coordinates);
            return;
        }

        function navError(err) {
            console.warn(`ERROR(${err.code}): ${err.message}`);
            setCityNamePending(false);
        }
        navigator.geolocation.getCurrentPosition(navSuccess, navError, navOptions);

    }, []);

    return (
        <div className="content-container">
            <div className="filter">
                <SearchForm data={data} onDataChange ={handleDataChange} />
            </div>
            { loading && 
                <div className="loading">
                    <h1>Loading...</h1>
                </div>
            }
            {error && 
            <div className="loading">
                <h1>Error occured.</h1>
            </div>
            }
            <div className="cards">
            {jobs.map((job,index) => {
                if (jobs.length === index + 1) {
                    return (
                    <Link to={{pathname:`/job/${job.id}`, state:job}} ref={lastJobElementRef} key={job.id} >
                        <Card hoverable>
                            <Skeleton loading={loading} avatar active>
                                <div className="image">
                                    { job.company_logo ? <img alt={job.company} src={job.company_logo} /> : <Skeleton.Image />}
                                </div>
                                <div className="top-level">
                                    <span>{moment(new Date(job.created_at)).fromNow()} -  {job.type} </span>
                                </div>
                                <br />
                                <Meta
                                    title={job.title}
                                    description={job.company}
                                />
                                <br />
                                <div className="bot-level">
                                    <span>{job.location}</span>
                                </div>
                            </Skeleton>
                        </Card>
                    </Link>
                    )
                } else {
                    return (
                    <Link to={{pathname:`/job/${job.id}`, state:job}} key={job.id} >
                        <Card hoverable>
                            <Skeleton loading={loading} avatar active>
                                <div className="image">
                                    { job.company_logo ? <img alt={job.company} src={job.company_logo} /> : <Skeleton.Image />}
                                </div>
                                <div className="top-level">
                                    <span>{moment(new Date(job.created_at)).fromNow()}  -  {job.type} </span>
                                </div>
                                <br />
                                <Meta
                                    title={job.title}
                                    description={job.company}
                                />
                                <br />
                                <div className="bot-level">
                                    <span>{job.location}</span>
                                </div>
                            </Skeleton>
                        </Card>
                    </Link>
                    )
                }
            })}
            {(jobs.length === 0 && !loading) && (
                <div className="no-records">
                    <Empty image={Empty.PRESENTED_IMAGE_SIMPLE} />
                </div>
            )}
            </div>
        </div>
    );
}

