import React from 'react'
import { Form } from 'react-bootstrap'
import './styles.scss';

export default function SearchForm({ data, onDataChange }) {
    return (
        <Form id='myForm'>
            <Form.Row className="form-row">
                <Form.Group className="filter-1">
                    <i className="fas fa-search"></i>
                    <Form.Control placeholder="Filter by title, companies, expertise ..." onChange={onDataChange} value={data.description || ''} name="description" type="text" />
                </Form.Group>
                <Form.Group className="filter-2">
                    <i className="fas fa-map-marker-alt"></i>
                    <Form.Control  placeholder="Filter by location ..." onChange={onDataChange} value={data.location || ''} name="location" type="text" />
                </Form.Group>
                <Form.Group className="filter-3">
                    <Form.Check onChange={onDataChange} value={data.full_time || false} name="full_time" id="full-time" label="Full Time Only" type="checkbox" className="mb-2" />
                </Form.Group>
                </Form.Row>
        </Form>
    )
}
