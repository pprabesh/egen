import React from "react";
import moment from 'moment';
import ReactMarkdown from 'react-markdown';
import { Skeleton } from 'antd';

import {
    useLocation,
} from "react-router-dom";

import './styles.scss';


export const Description =  ({props}) => {

    let location = useLocation();
    const job = location.state

    return (
        <div className="description">
            <div className="top">
                <div className="logo">
                    { job.company_logo ? <img alt={job.company} src={job.company_logo} /> : <Skeleton.Image />}
                </div>
                <div className="info">
                    <span> {job.company} </span>
                </div>
                <div className="link-btn">
                    <a href={job.company_url} target= "_blank" rel="noopener noreferrer"><button>Company Site</button></a>
                </div>
            </div>
            <div className="des-body">
            <div className="mid">
                <div className="mid-left">
                    <span className="time">{moment(new Date(job.created_at)).fromNow()} --- {job.type}</span>
                    <label>{job.title}</label>
                    <span className="location">{job.location}</span>
                </div>
                <div className="mid-right">
                    <div className="apply-btn">
                        <button>Apply Now</button>
                    </div>
                </div>
            </div>
            <div className="para">
                <ReactMarkdown>{job.description}</ReactMarkdown>
            </div>
            <div className="bot">
                <span> How to Apply </span>
                <ReactMarkdown>{job.how_to_apply}</ReactMarkdown>
            </div>
            <div className="proxy-2" />
            <div className="footer">
                <div className="info">
                    <span> {job.title} </span>
                    <label> {job.company} </label>
                </div>
                <div className="apply-btn">
                    <button>Apply Now</button>
                </div>
            </div>
            </div>
        </div>
    );
};
